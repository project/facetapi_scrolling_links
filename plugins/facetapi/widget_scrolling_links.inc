<?php
/**
 * @file
 * The facetapi_scrolling_links widget plugin classes.
 */

/**
 * Widget that renders facets as a list of clickable links and scroll.
 *
 * Links make it easy for users to narrow down their search results by clicking
 * on them. The render arrays use theme_item_list() to generate the HTML markup
 * and will include an ID in the URL so we can scroll to the same build.
 */
class FacetapiScrollingLinksWidget extends FacetapiWidgetLinks {

  /**
   * Renders the form.
   */
  public function execute() {
    // Get the elements.
    $elements = &$this->build[$this->facet['field alias']];

    // Append the ID as a hash value in the query.
    foreach ($elements as &$element) {
      $element['#query']['#'] = $this->build['#attributes']['id'];
    }

    // Sets each item's theme hook, builds item list.
    $this->setThemeHooks($elements);
    $elements = array(
      '#theme' => 'item_list',
      '#items' => $this->buildListItems($elements),
      '#attributes' => $this->build['#attributes'],
    );
  }

  /**
   * Recursive function that sets each item's theme hook.
   *
   * The individual items will be rendered by different theme hooks depending on
   * whether or not they are active.
   *
   * @param array &$build
   *   A render array containing the facet items.
   *
   * @return FacetapiWidget
   *   An instance of this class.
   *
   * @see theme_facetapi_link_active()
   * @see theme_facetapi_link_inactive()
   */
  protected function setThemeHooks(array &$build) {
    foreach ($build as &$item) {
      $item['#theme'] = ($item['#active']) ? 'facetapi_scrolling_link_active' : 'facetapi_scrolling_link_inactive';
      if (!empty($item['#item_children'])) {
        $this->setThemeHooks($item['#item_children']);
      }
    }
    return $this;
  }

}

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Facet API Scrolling links module provides a new display widget for Facets
called "Links with scroll". It will show a throbber/spinner after clicking one
of the facet links and scroll the screen to the facet on next page load.

Background: It's a very simple module that's developed because I worked on a
project where the client didn't like starting at the top of the page every
time they selected a facet on mobile devices. This module is a simple
alternative to fetching new results with AJAX.


REQUIREMENTS
------------

This module requires the following modules:

 * [Facet API](https://www.drupal.org/project/facetapi "Facet API")


RECOMMENDED MODULES
-------------------

 * Search API (https://www.drupal.org/project/search_api):
   Is a search module that provides facet support and integration with
   multiple search servers like Apache Solr.

 * Search API Solr (https://www.drupal.org/project/search_api_solr):
   Integrates with Search API and provides a Apache Solr server support.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

 * A minimum screen width before the throbber/scroll functionality activates.

 * Custom whitespace above the element used to create the scroll offset.

 * A list of options with different "scroll to" elements (if the "parent"
   option is selected, you can then provide your own selector, if you don't
   use the default facet-blocks, created by the Facet API module).


MAINTAINERS
-----------

Current maintainers:
 * André Philip Kallehauge (kallehauge) - https://drupal.org/user/2812383

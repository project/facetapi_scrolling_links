/**
 * @file
 * JS file for the Facet API Scrolling links module.
 */

(function ($) {
  "use strict";

  /**
   * Define Facetapi Scrolling Links behaviors object.
   */
  Drupal.facetapiScrollingLinks = {};

  /**
   * Handles our base logic to go to facets after page load.
   */
  Drupal.behaviors.facetapiScrollingLinksBaseLogic = {
    attach: function (context, settings) {
      $(window).load(function () {
        // Check if the window is within "activation width" or 0 (always active).
        var activationWidth = parseInt(Drupal.settings.facetapiScrollingLinks.activationWidth, 10);
        if ($(this).width() <= activationWidth || activationWidth === 0) {
          // Use a helper function to fetch the facet ID from the URL.
          var $element = $('#' + Drupal.facetapiScrollingLinks.getQueryParam('#'), context);

          // Scroll to the element, if it exists in the DOM.
          if ($element.get(0)) {
            // Set the offset by finding the "Scroll to" element and deduct the
            // whitespace provided on the configuration page.
            var offset;
            switch (Drupal.settings.facetapiScrollingLinks.scrollTo) {
              case 'list':
                offset = ($element.offset().top) - parseInt(Drupal.settings.facetapiScrollingLinks.whitespaceTop, 10);
                break;

              case 'parent_selector':
                offset = ($element.closest(Drupal.settings.facetapiScrollingLinks.parentSelector).offset().top) - parseInt(Drupal.settings.facetapiScrollingLinks.whitespaceTop, 10);
                break;

              default:
                offset = 0;
                break;
            }

            $('html, body', context).animate({
              scrollTop: offset
            }, 0);
          }

          // Activate throbber when clicking on a scrolling facet link.
          $('.facetapi-facetapi-scrolling-links a', context).click(function () {
            $('body', context).append($('<div>', {
              id: 'facetapi_scrolling_links_throbber'
            }));
          });
        }
      });
    }
  };

  /**
   * Get specific query parameter from the URL.
   *
   * Inspired by http://stackoverflow.com/a/6637898.
   *
   * @param param
   *
   * @returns string|boolean
   */
  Drupal.facetapiScrollingLinks.getQueryParam = function (param) {
    // Get the query parameters and clean it up.
    var query = decodeURI(window.location.search.substring(1)).replace(/%23/g, '#');

    // Split up the string and look for the requested parameter.
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
      var pair = vars[i].split('=');
      if (pair[0] === param) {
        return pair[1];
      }
    }

    // Return false if there's no match.
    return false;
  };

})(jQuery);
